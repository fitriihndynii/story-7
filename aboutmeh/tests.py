from django.test import TestCase,Client
from django.urls import reverse, resolve
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from .views import aboutmeh
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains

# Create your tests here.
class AboutmehTest(TestCase):
    def test_apakah_terdapat_url_aboutmeh(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_apakah_fungsi_aboutmeh_dijalankan(self):
        response = resolve(reverse('aboutmeh'))
        self.assertEqual(response.func, aboutmeh)

    def test_apakah_template_html_yang_digunakan_benar(self):
        response = Client().get('')
        self.assertTemplateUsed('aboutmeh.html')
    
    def test_apakah_terdapat_toggle_switch_mode(self):
        response = Client().get('')
        content = response.content.decode('utf-8')
        self.assertIn('toggle', content)
    
    def test_apakah_terdapat_profil(self):
        response = Client().get('')
        content = response.content.decode('utf-8')
        self.assertIn('profil', content)

    def test_apakah_terdapat_accordion(self):
        response = Client().get('')
        content = response.content.decode('utf-8')
        self.assertIn('accordion', content)
        self.assertIn('card', content)
        self.assertIn('card-body', content)

class AboutmehFunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(chrome_options=options)
    
    def tearDown(self):
        self.selenium.close()

    def test_apakah_toggle_switch_dapat_menggganti_tema(self):
        self.selenium.get(self.live_server_url)
        toggle = self.selenium.find_element_by_class_name("slider")

        #tema default awalnya ialah light dan menggunakan file style.css
        self.assertIn('light', self.selenium.page_source)
        self.assertIn('style.css', self.selenium.page_source)
        time.sleep(3)

        #Ketika toggle button diklik, maka tema akan berubah menjadi dark
        # dan menggunakan file nightstyle.css
        toggle.click()
        self.assertIn('dark', self.selenium.page_source)
        self.assertIn('nightstyle.css', self.selenium.page_source)
        time.sleep(3)

        #Ketika toggle diklik lagi, tema akan kembali ke light
        #dan menggunakkan file style.css
        toggle.click()
        self.assertIn('light', self.selenium.page_source)
        self.assertIn('style.css', self.selenium.page_source)
        time.sleep(3)

    def test_apakah_accordion_activity_dapat_diklik(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        #Tampilan awal website, semua accordion dalam keadaan tertutup
        self.assertIn('display: none', selenium.page_source)
        time.sleep(3)

        #Saat card-header accordion di klik, maka accordion akan terbuka
        activity_content = selenium.find_element_by_name("activity")
        activity_content.click()
        self.assertNotIn('name= "activity" style= "display: none;"', selenium.page_source)

    def test_apakah_accordion_experience_dapat_diklik(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        #Tampilan awal website, semua accordion dalam keadaan tertutup
        self.assertIn('display: none', selenium.page_source)
        time.sleep(3)

        #Saat card-header accordion di klik, maka accordion akan terbuka
        experience_content = selenium.find_element_by_name("experience")
        experience_content.click()
        self.assertNotIn('name= "experience" style= "display: none;"', selenium.page_source)