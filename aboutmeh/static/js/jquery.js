$(document).ready(function(){
    $(".card-body").hide()
    $(".load").remove()
})

$(".slider").click(function(){
    if($(this).hasClass("light")){
        $(this).removeClass("light")
        $(this).addClass("dark")
        $(".style").attr("href","static/css/nightstyle.css")
        $(".icon").attr("href","https://files.catbox.moe/g9tc5n.png")
        $(".logo").attr("src","https://files.catbox.moe/g9tc5n.png")
    }else{
        $(this).removeClass("dark")
        $(this).addClass("light")
        $(".style").attr("href","static/css/style.css")
        $(".icon").attr("href","https://files.catbox.moe/qoyk7z.png")
        $(".logo").attr("src","https://files.catbox.moe/qoyk7z.png")
    }
})

$(".card-header").click(function(){
    if($(this).hasClass("active")){
        $(this).removeClass("active")
        $(this).next(".card-body").slideUp()
    }else{
        $(".card .card-body").slideUp()
        $(".card .card-body").removeClass("active")
        $(this).addClass("active")
        $(this).next(".card-body").slideDown()
    }
})